#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
import re
#import random
from iching_constants import ichingk
import numpy as np

def print_lines(hex,mode='cli',hex_org=0):
    #mode, if cli or bot style printing
    lines={6: '6  x ===== =====',
           7: '7    ===========',
           8: '8    ===== =====',
           9: '9  o ==========='};
    
    lines_bot={6: '`6  x ===== =====`',
               7: '`7    ===========`',
               8: '`8    ===== =====`',
               9: '`9  o ===========`'};
    
    a=ichingk()
    
    if hex_org != 0:
        print(a.lines[hex_org], a.names[a.lines[hex_org]])
    else:
        print(a.lines[hex], a.names[a.lines[hex]])

    for i in range(5,-1,-1):
        #print lines[hex[i]]
        print(lines[int(hex[i])])
    print('')

mode='cli'
mode='bot'

count=0
#hexagram_l=[]
hexagram=''

while (count <=5):
   #first=random.randint(2,3)
   first=np.random.choice([2,3])
   second=np.random.choice([2,3])
   third=np.random.choice([2,3])
   line=first+second+third
   #hexagram_l.append(line)
   hexagram += str(line)
   count = count + 1

#mutation - 
#
#check if mutates
#check where and if mutate, keep track of the mutating lines
#deliver hexagram, mutation, lines that have mutated
# 9 ==> 8
# 6 ==> 7

#check if there are mutating lines
if (hexagram.find('9') != -1 ) or (hexagram.find('6') != -1 ):
#check where mutation
    ninem=''
    sixm=''
    for m in re.finditer('9', hexagram): #return where the occurence happens
        ninem += str(m.end())
    for m in re.finditer('6', hexagram):
        sixm += str(m.end())
###################

#get hexagram value (reverse the mutation), use hexagram copy, to preserve var integrity for later
    hexagram_org=hexagram
    hexagram_org=str.replace(hexagram_org,'9', '7')
    hexagram_org=str.replace(hexagram_org,'6', '8')
#send to the print cueue
    print_lines (hexagram,mode,hexagram_org)

#make the mutation 
    hexagram=str.replace(hexagram,'9', '8')
    hexagram=str.replace(hexagram,'6', '7')
#send to the print cueue
    print_lines (hexagram,mode)

else:
    print_lines (hexagram,mode)
